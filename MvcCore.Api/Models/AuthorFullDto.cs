﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcCore.Api.Models
{
    public class AuthorFullDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MainCategory { get; set; }
        public DateTimeOffset DateOfBirth { get; set; }
    }
}
