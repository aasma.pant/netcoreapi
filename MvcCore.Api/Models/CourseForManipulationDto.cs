﻿using MvcCore.Api.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MvcCore.Api.Models
{
    [CourseTitleMustBeDifferentFromDescription(ErrorMessage = "Title must be different from description.")]
    public abstract class CourseForManipulationDto
    {
        [Required(ErrorMessage = "You should fill out title.")]
        [MaxLength(100, ErrorMessage = "Title should not have more than 100 characters.")]
        public string Title { get; set; }
                
        [MaxLength(1500, ErrorMessage = "Description should not have more than 1500 characters.")]
        public virtual string Description { get; set; }
    }
}
